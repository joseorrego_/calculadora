package gt.edu.jsamayoa_2009190kinal.calculator;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import org.w3c.dom.Text;


public class MainActivity extends ActionBarActivity {

    private String datosGene;
    private TextView tv;
    Double operacion1, operacion2, resultado;
    String operacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        datosGene = "";
        tv = (TextView) findViewById(R.id.textView);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void Button0 (View miView){
        tv.setText(tv.getText()+ "0");
        datosGene += "0";
    }
    public void Button1 (View miView){
        tv.setText(tv.getText()+ "1");
        datosGene += "1";
    }
    public void Button2 (View miView){
        tv.setText(tv.getText()+ "2");
        datosGene += "2";
    }
    public void Button3 (View miView){
        tv.setText(tv.getText()+ "3");
        datosGene += "3";
    }
    public void Button4 (View miView){
        tv.setText(tv.getText()+ "4");
        datosGene += "4";
    }
    public void Button5 (View miView){
        tv.setText(tv.getText()+ "5");
        datosGene += "5";
    }
    public void Button6 (View miView){
        tv.setText(tv.getText()+ "6");
        datosGene += "6";
    }
    public void Button7 (View miView){
        tv.setText(tv.getText()+ "7");
        datosGene += "7";
    }
    public void Button8 (View miView){
        tv.setText(tv.getText()+ "8");
        datosGene += "8";
    }
    public void Button9 (View miView){
        tv.setText(tv.getText()+ "9");
        datosGene += "9";
    }
    public void ButtonPunto (View miView){

        tv.setText(tv.getText()+ ".");
        datosGene += ".";
    }
    public void ButtonLimpieza(View miView){
        operacion1= 0.0;
        operacion2= 0.0;
        tv.setText("");
        datosGene = "";
    }

    public void ButtonSuma (View miView){
        datosGene += "+";
        tv.setText(datosGene);
        operacion = "+";
    }
    public void ButtonResta (View miView){
        datosGene += "-";
        tv.setText(datosGene);
        operacion = "-";
    }
    public void ButtonMultiplicacion (View miView){
        datosGene += "*";
        tv.setText(datosGene);
        operacion = "*";
    }
    public void ButtonDivision (View miView){
        datosGene += "/";
        tv.setText(datosGene);
        operacion = "/";
    }
    public void ButtonIgual(View miView)
    {
        if (operacion.length() > 0){
            operar(datosGene,operacion);
        }else {
            datosGene = "";
        }
    }
    public void operar(String datosT,String operacionT)
    {

        try {
            if (operacionT.equals("+"))
            {
                String[] parts = datosT.split("\\+");
                String part1 = parts[0];
                String part2 = parts[1];


                double x = Double.parseDouble(part1);
                double y = Double.parseDouble(part2);

                double totalO = x + y;

                datosGene = String.valueOf(totalO);

                tv.setText(datosGene);

                Toast toast = Toast.makeText(getApplicationContext(), datosGene, Toast.LENGTH_SHORT);
                toast.show();
                operacion = "";

            }else if (operacionT.equals("-"))
            {
                String[] parts = datosT.split("\\-");
                String part1 = parts[0];
                String part2 = parts[1];


                double x = Double.parseDouble(part1);
                double y = Double.parseDouble(part2);

                double totalO = x - y;

                datosGene = String.valueOf(totalO);

                tv.setText(datosGene);

                Toast toast = Toast.makeText(getApplicationContext(), datosGene, Toast.LENGTH_SHORT);
                toast.show();
                operacion = "";

            }else if (operacionT.equals("*"))
            {
                String[] parts = datosT.split("\\*");
                String part1 = parts[0];
                String part2 = parts[1];


                double x = Double.parseDouble(part1);
                double y = Double.parseDouble(part2);

                double totalO = x * y;

                datosGene = String.valueOf(totalO);

                tv.setText(datosGene);

                Toast toast = Toast.makeText(getApplicationContext(), datosGene, Toast.LENGTH_SHORT);
                toast.show();
                operacion = "";

            }else if (operacionT.equals("/"))
            {
                String[] parts = datosT.split("\\/");
                String part1 = parts[0];
                String part2 = parts[1];


                double x = Double.parseDouble(part1);
                double y = Double.parseDouble(part2);

                double totalO = x / y;

                datosGene = String.valueOf(totalO);

                tv.setText(datosGene);

                Toast toast = Toast.makeText(getApplicationContext(), datosGene, Toast.LENGTH_SHORT);
                toast.show();
                operacion = "";

            }
        }catch (Exception e){

        }

    }
}
